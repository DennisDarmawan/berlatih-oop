<?php 

	require_once "animal.php";
	require_once "Frog.php";
	require_once "Ape.php";

	$sheep = new Animal("Shaun");
	
	$sungokong = new Ape("Kera Sakti");
	$yell = new Ape("Auoo");

	$frog = new Frog("Buduk", "4", "false");
	$jump = new Frog("hop hop");

	// Sheep
	echo "Name : " . $sheep->get_name() . "<br>";
	echo "Legs : " . $sheep->get_legs() . "<br>";
	echo "Cold Blooded : " . $sheep->get_cold_blooded() . "<br><br>";
	
	// Ape
	echo "Name : " . $sungokong->get_name() . "<br>";
	echo "Legs : " . $sungokong->get_legs() . "<br>";
	echo "Cold Blooded : " . $sungokong->get_cold_blooded() . "<br>";
	echo $yell->get_yell() . "<br><br>";

	// Frog
	echo "Name : " . $frog->get_name() . "<br>";
	echo "Legs : " . $frog->get_legs() . "<br>";
	echo "Cold Blooded : " . $frog->get_cold_blooded() . "<br>";
	echo $jump->get_jump() . "<br><br>";
 ?>